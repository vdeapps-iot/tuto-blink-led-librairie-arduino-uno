# Projet Blink led avec librairie et tests unitaires

Ce projet a pour but d'expliquer comment créer une librairie
qui contiendra toute la logique et process pour faire clignoter la led.

Le dossier 'src' contient le programme principal, qui initialisera la librairie et
exécutera le processus de clignotement.

Le dossier 'lib/BlinkLed' contient la définition de la librairie BlinkLed et la description de ses méthodes.

L'utilisation de vscode (ou codium pour mon cas) ainsi que le plugins Platform.io simplifie grandement le visuel et la compilation du projet.

Pour lancer les tests, sélectionnez l'icône "Test".
