#include <Arduino.h>
#include <unity.h>
#include "BlinkLed.h"
/**
 * 
 * Exemple de test pour tester le bon clignotement de la led interne 13
 * 
 * @link https://docs.platformio.org/en/latest/plus/unit-testing.html
 * 
 */

uint8_t i = 0;
uint8_t max_blinks = 5;

// void setUp(void) {
// // set stuff up here
// }

// void tearDown(void) {
// // clean stuff up here
// }

void test_led_builtin_pin_number(void) {
    TEST_ASSERT_EQUAL(LED_BUILTIN, 13);
}

void test_led_state_high(void) {
    digitalWrite(LED_BUILTIN, HIGH);
    TEST_ASSERT_EQUAL(digitalRead(LED_BUILTIN), HIGH);
}

void test_led_state_low(void) {
    digitalWrite(LED_BUILTIN, LOW);
    TEST_ASSERT_EQUAL(digitalRead(LED_BUILTIN), LOW);
}

void test_setup_serialDisabled(void){
    BlinkLed bl(LED_BUILTIN, 0);
    TEST_ASSERT_EQUAL(0, bl.getSerialBaud());
}
void test_setup_serialEnabled(void){
    BlinkLed bl(LED_BUILTIN, 115200);
    TEST_ASSERT_EQUAL(115200, bl.getSerialBaud());
}

/**
 * setup
 */
void setup() {
    UNITY_BEGIN();
    RUN_TEST(test_led_builtin_pin_number);
    RUN_TEST(test_setup_serialDisabled);
    RUN_TEST(test_setup_serialEnabled);

    pinMode(LED_BUILTIN, OUTPUT);
}

/**
 * loop
 */
void loop() {
    if (i < max_blinks)
    {
        RUN_TEST(test_led_state_high);
        delay(500);
        RUN_TEST(test_led_state_low);
        delay(500);
        i++;
    }
    else if (i == max_blinks) {
      UNITY_END(); // stop unit testing
    }
}
