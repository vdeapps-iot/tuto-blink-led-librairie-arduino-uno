#include "BlinkLed.h"

/**
 * 
 * Class pour faire clignoter la led d'un arduino uno
 * 
 * L'utilisation de "this->"" n'est pas obligatoire mais permet visuellement
 * de voir si c'est une méthode globale de la librairie de l'arduino ou bien
 * une méthode de l'objet 
 */

/**
 * Constructor
 * Initialise les valeurs
 * @param pinLed Pin led ID
 * @param baud Vitesse de communication. set 0 to disable Serial
 */
BlinkLed::BlinkLed(unsigned int pinLed, unsigned long baud)
{
    this->_pinLed = pinLed;
    this->_compteur = 0;
    this->_baud = baud;
    this->_delay_show = 1000;
    this->_delay_hide = 1000;
    this->_state = 0;
}

/**
 * setup method to call in setup() main program
 */
void BlinkLed::setup(){
    this->setup(BL_DELAY_SHOW, BL_DELAY_HIDE);
}

/**
 * setup method to call in setup() main program
 * @param delayShowMs delay to show led
 * @param delayHideMs delay to hide led
 */
void BlinkLed::setup(unsigned long delayShowMs, unsigned long delayHideMs){
    
    if(this->isSerialEnable()){
        Serial.begin( this->getSerialBaud() );
        this->println("TEST BlinkLed - Show "+String(delayShowMs)+"ms, Hide "+String(delayHideMs)+" ms, Serial " + (this->isSerialEnable() ? "enabled":"disabled") );
    }

    pinMode(_pinLed, OUTPUT);
    this->off();
    this->setDelay(delayShowMs, delayHideMs);
}

/**
 * Configure la durée d'affichage de la led
 * @param ms Delai en ms
 */
void BlinkLed::setDelayShow(unsigned long ms)
{
    _delay_show = ms;
    this->println("Set delay to show = " + String(_delay_show) + " ms");
}

/**
 * Configure la durée d'extinction de la led
 * @param ms Delai en ms
 */
void BlinkLed::setDelayHide(unsigned long ms)
{
    _delay_hide = ms;
    this->println("Set delay to hide = " + String(_delay_hide) + " ms");
}

/**
 * Configure la durée d'affichage et d'extinction de la led
 * @param show Delai en ms
 * @param hide Delai en ms
 */
void BlinkLed::setDelay(unsigned long show, unsigned long hide){
    this->setDelayShow(show);
    this->setDelayHide(hide);
}

/**
 * Turn on light if not already on
 * @return bool true: state changed; false: state already changed
 */
bool BlinkLed::on(){
    if (this->_state == 1){
        return false;
    }
    digitalWrite(_pinLed, HIGH);
    this->_state = 1;
    return true;
}

/**
 * Turn off light if not already off
 * @return bool true: state changed; false: state already changed
 */
bool BlinkLed::off(){
    if (this->_state == 0){
        return false;
    }
    digitalWrite(_pinLed, LOW);
    this->_state = 0;
    return true;
}

/**
 * Exécute l'action
 * @return Etat du compteur
 */
unsigned long BlinkLed::run()
{
    if (_delay_hide <0 || _delay_show <0){
        this->println("Bad configuration to delay");
        return 0;
    }

    this->on();
    delay(_delay_show);            // wait for a second
    this->off();
    delay(_delay_hide);            // wait for a second

    _compteur++;

    this->println("Compteur = " + String(_compteur));
    
    if (_compteur == 10){
        _compteur = 0;
        this->println("Restart compteur");
    }

    return _compteur;
}

/**
 * Test if Serial is enable
 * @return bool
 */
bool BlinkLed::isSerialEnable(){
    return this->_baud != 0;
}

/**
 * Print message to Serial
 * @param message String
 */
void BlinkLed::println(String message){
    if( this->isSerialEnable() ){
        Serial.println(message);
    }
}

/**
 * Return the Serial baud
 * @return unsigned long
 */
unsigned long BlinkLed::getSerialBaud(){
    return _baud;
}
