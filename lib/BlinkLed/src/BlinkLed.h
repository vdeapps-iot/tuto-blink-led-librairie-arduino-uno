#include <Arduino.h>

#ifndef BlinkLed_h
#define BlinkLed_h

#define BL_DELAY_SHOW 1000
#define BL_DELAY_HIDE 500

class BlinkLed
{
private:
    unsigned int _pinLed;
    unsigned long _compteur;
    unsigned long _baud;
    unsigned long _delay_show;
    unsigned long _delay_hide;
    unsigned int _state;

public:
    BlinkLed(unsigned int pinLed, unsigned long baud);
    void setup();
    void setup(unsigned long delayShowMs, unsigned long delayHideMs);
    void setDelayShow(unsigned long ms);
    void setDelayHide(unsigned long ms);
    void setDelay(unsigned long show, unsigned long hide);
    bool on();
    bool off();
    unsigned long run();
    bool isSerialEnable();
    void println(String message);
    unsigned long getSerialBaud();
};

#endif